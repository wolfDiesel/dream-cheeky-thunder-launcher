﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using CheekyWPF.Annotations;
using UsbLibrary;

namespace RocketPost 
{
    public class RocketLauncher : INotifyPropertyChanged
    {
        private bool DevicePresent;
        private int RotationSpeed = 50;
        //Bytes used in command
        private byte[] UP;
        private byte[] RIGHT;
        private byte[] LEFT;
        private byte[] DOWN;

        private byte[] FIRE;
        private byte[] STOP;
        private byte[] LED_OFF;
        private byte[] LED_ON;

        protected UsbHidPort Device;

        public int Speed { get=>RotationSpeed;
            set
            {
                RotationSpeed = value;
                OnPropertyChanged(nameof(RotationSpeed));
                OnPropertyChanged(nameof(RotationSpeed));
                OnPropertyChanged(nameof(RotationSpeed));

            }
        }

        public bool Online { get => DevicePresent; private set
            {
                DevicePresent = value;
                OnPropertyChanged(nameof(Online));
            }
        }

        public RocketLauncher()
        {

            this.UP = new byte[10];
            this.UP[1] = 2;
            this.UP[2] = 2;

            this.DOWN = new byte[10];
            this.DOWN[1] = 2;
            this.DOWN[2] = 1;

            this.LEFT = new byte[10];
            this.LEFT[1] = 2;
            this.LEFT[2] = 4;

            this.RIGHT = new byte[10];
            this.RIGHT[1] = 2;
            this.RIGHT[2] = 8;

            this.FIRE = new byte[10];
            this.FIRE[1] = 2;
            this.FIRE[2] = 0x10;

            this.STOP = new byte[10];
            this.STOP[1] = 2;
            this.STOP[2] = 0x20;

            this.LED_ON = new byte[9];
            this.LED_ON[1] = 3;
            this.LED_ON[2] = 1;

            this.LED_OFF = new byte[9];
            this.LED_OFF[1] = 3;

            this.Device = new UsbHidPort();
            this.Device.VendorId = 0x2123;//chheky thunder Vendor
            this.Device.ProductId = 0x1010;//chheky thunder PdcID
            this.Device.OnDeviceArrived += this.USB_OnSpecifiedDeviceArrived;
            this.Device.OnSpecifiedDeviceRemoved += this.USB_OnSpecifiedDeviceRemoved;
            this.Device.OnDataRecieved += new DataRecievedEventHandler(this.USB_OnDataRecieved);
            this.Device.OnSpecifiedDeviceArrived += new EventHandler(this.USB_OnSpecifiedDeviceArrived);
            

            IntPtr handle = new IntPtr();
            this.Device.RegisterHandle(handle);
            this.Device.CheckDevicePresent();
            Device.SpecifiedDevice.OnDeviceRemoved += this.USB_OnSpecifiedDeviceRemoved;
        }

        public void command_Stop()
        {
            this.SendUSBData(this.STOP);
        }

        public void command_Right()
        {
            this.moveMissileLauncher(this.RIGHT, RotationSpeed);
        }

        public void command_Left()
        {
            this.moveMissileLauncher(this.LEFT, RotationSpeed);
        }

        public void command_Up()
        {
            this.moveMissileLauncher(this.UP, RotationSpeed);
        }

        public void command_Down()
        {
            this.moveMissileLauncher(this.DOWN, RotationSpeed);
        }

        public void command_Fire()
        {
            this.moveMissileLauncher(this.FIRE, 5000);
        }

        public void command_switchLED(Boolean turnOn)
        {
            if (Online)
            {
                if (turnOn)
                {
                    this.SendUSBData(this.LED_ON);
                }
                else
                {
                    this.SendUSBData(this.LED_OFF);
                }
                this.SendUSBData(this.STOP);
            }
        }


        public void command_reset()
        {
            if (Online)
            {
                this.moveMissileLauncher(this.LEFT, 5500);
                this.moveMissileLauncher(this.RIGHT, 2750);
                this.moveMissileLauncher(this.UP, 2000);
                this.moveMissileLauncher(this.DOWN, 500);
            }
        }

        private void moveMissileLauncher(byte[] Data, int interval)
        {
            if (Online)
            {
                this.command_switchLED(true);
                this.SendUSBData(Data);
                Thread.Sleep(interval);
                this.SendUSBData(this.STOP);
                this.command_switchLED(false);
            }
        }

        private void SendUSBData(byte[] Data)
        {
            Device.SpecifiedDevice?.SendData(Data);
        }


        private void USB_OnDataRecieved(object sender, DataRecievedEventArgs args)
        {

        }

        private void USB_OnSpecifiedDeviceArrived(object sender, EventArgs e)
        {
            Device.CheckDevicePresent();
            this.Online = true;
            if (this.Device.ProductId == 0x1010)
            {
                this.command_switchLED(true);
            }
        }

        private void USB_OnSpecifiedDeviceRemoved(object sender, EventArgs e)
        {
            this.Online = false;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}


