﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheekyWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public RocketPost.RocketLauncher lnchr = new RocketPost.RocketLauncher();
        
        public MainWindow()
        {
            EventManager.RegisterClassHandler(typeof(Window), Keyboard.KeyDownEvent, new KeyEventHandler(handleKeys), true);
            InitializeComponent();
            rc1.DataContext = lnchr;
            SpeedSlider.Focusable = false;
            SpeedSlider.DataContext = lnchr;
            Speed_label_value.DataContext = lnchr;

        }


        private void handleKeys(object sender, KeyEventArgs e)
        {
            if (rb_keyboard.IsChecked == true)
            {
                switch (e.Key)
                {
                    case Key.Up:
                        this.upBtn_Click(this, e);
                        break;
                    case Key.Down:
                        this.btnDown_Click(this, e);
                        break;
                    case Key.Left:
                        this.btnLeft_Click(this, e);
                        break;
                    case Key.Right:
                        this.btnRight_Click(this, e);
                        break;
                    case Key.Space:
                        this.Button_Click(this, e);
                        break;
                }
            }

        }

        private void btnRight_Click(object sender, RoutedEventArgs e)
        {
            lnchr.command_Right();
        }

        private void btnLeft_Click(object sender, RoutedEventArgs e)
        {
            lnchr.command_Left();
        }

        private void upBtn_Click(object sender, RoutedEventArgs e)
        {
            lnchr.command_Up();
        }

        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            lnchr.command_Down();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            lnchr.command_Fire();
        }
    }
}
